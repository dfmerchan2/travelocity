# Automatización de pruebas del módulo de vuelos en Travelocity

Automatización de pruebas funcionales al proceso de búsqueda de un vuelo a reservar en la agencia de viajes Tevelocity.

## Technological utilizada

El desarrollo de la automatización fue desarrollo con las siguientes tecnologías.

* **Lenguajes:** Java - Gherkin
* **Framework:** Serenity BDD - Selenium - Cucumber
* **Patron de diseño:** Screenplay
* **Gestor de dependencias:** Gradle
* **Versiona miento de código:** Git
* **Libraries:** [Java Faker](https://github.com/DiUS/java-faker) para la generación de data Random

## Pre-requisitos

* Tener instalado el [JDK](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) Java
  Development Kit como mínimo en la versión 1.8
* Tener instalado [Gradle](https://gradle.org/releases/)
* Tener instalado los siguientes navegadores, dado que la automatización tiene un random de driver para ejecutar las
  pruebas en diferentes navegadores.
    * [Chrome](https://www.google.com/intl/es/chrome/)
    * [Edge](https://www.microsoft.com/es-es/edge)
    * [Mozilla](https://www.mozilla.org/es-ES/firefox/new/)

## Instalación

* Clonar el proyecto de la rama [master](https://bitbucket.org/dfmerchan2/travelocity/src/master/) ejecutando el
  siguiente comando.
```
git clone https://bitbucket.org/dfmerchan2/travelocity.git
```