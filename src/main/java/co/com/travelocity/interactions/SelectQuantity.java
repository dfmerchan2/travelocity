package co.com.travelocity.interactions;

import co.com.travelocity.questions.ObtainList;
import com.github.javafaker.Faker;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.*;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class SelectQuantity implements Interaction {
    private Faker faker = new Faker();
    private Target lstAges = null;
    private Target lblText;
    private Target optBtnAges;
    private Target btnAges;
    private Target btnIncrease;
    private int quantity = 0;

    public SelectQuantity(Target lblText, Target btnIncrease) {
        this.lblText = lblText;
        this.btnIncrease = btnIncrease;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        int i = 0;
        while (!lblText.resolveFor(actor).getValue().equals(Integer.toString(quantity))) {
            actor.attemptsTo(MoveMouse.to(btnIncrease).andThen(Actions::click));
            if (lstAges != null) {
                List<String> ages = actor.asksFor(ObtainList.is(lstAges.of(String.valueOf(i))));
                String age = ages.get(faker.random().nextInt(1, ages.size() - 1));
                actor.attemptsTo(
                        MoveMouse.to(btnAges.of(String.valueOf(i))).andThen(Actions::click),
                        Wait.forAbout(2).seconds(),
                        Click.on(optBtnAges.of(String.valueOf(i), age))
                );
            }
            i++;
        }
    }

    public static SelectQuantity ofTravelers(Target lblText, Target btnIncrease) {
        return new SelectQuantity(lblText, btnIncrease);
    }


    public SelectQuantity withQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    public SelectQuantity withListAges(Target lstAges) {
        this.lstAges = lstAges;
        return this;
    }

    public SelectQuantity withButtonAges(Target btnAges) {
        this.btnAges = btnAges;
        return this;
    }

    public SelectQuantity withOptionButtonAges(Target optBtnAges) {
        this.optBtnAges = optBtnAges;
        return this;
    }
}
