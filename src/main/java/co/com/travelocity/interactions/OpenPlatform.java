package co.com.travelocity.interactions;


import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.actions.Open;

import static co.com.travelocity.utils.Urls.URL_TRAVELOCITY;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class OpenPlatform implements Interaction {
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Open.url(URL_TRAVELOCITY.getUrl())
        );
    }

    public static Performable ofTravelocity() {
        return instrumented(OpenPlatform.class);
    }
}
