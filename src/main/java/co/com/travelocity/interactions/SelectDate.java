package co.com.travelocity.interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.MoveMouse;
import net.serenitybdd.screenplay.conditions.Check;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.interactions.Actions;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static co.com.travelocity.userinterfaces.Calendar.*;

public class SelectDate implements Interaction {

    private final Target target;
    private String date;

    public SelectDate(Target target) {
        this.target = target;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        List<String> dates = Arrays.stream(date.split("-")).collect(Collectors.toList());
        String sDate = dates.get(0) + " " + dates.get(1);
        actor.attemptsTo(
                Check.whether(!BTN_NEXT.resolveFor(actor).isPresent())
                        .andIfSo(Click.on(target)));
        while (!LBL_DATE.resolveFor(actor).getText().equals(sDate)) {
            actor.attemptsTo(
                    MoveMouse.to(BTN_NEXT).andThen(Actions::click),
                    Click.on(BTN_CALENDAR_DAY.of(dates.get(2))));
        }
    }

    public static SelectDate onCalendar(Target target) {
        return new SelectDate(target);
    }

    public SelectDate withDate(String date) {
        this.date = date;
        return this;
    }
}
