package co.com.travelocity.utils;

import com.github.javafaker.Faker;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import static co.com.travelocity.utils.Constant.*;

public class Utilities {

    private static final Faker faker = new Faker();

    public static String getRandomDriver() {
        String[] drivers = {"chrome", "firefox", "edge"};
        int accountant = faker.random().nextInt(drivers.length);
        return drivers[accountant];
    }

    public static String getDate(int moreDays) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT, new Locale("en"));
        LocalDateTime currentDate = LocalDateTime.now();
        return dateTimeFormatter.format(currentDate.plusDays(moreDays).plusMonths(2));
    }

    private Utilities() {
    }
}
