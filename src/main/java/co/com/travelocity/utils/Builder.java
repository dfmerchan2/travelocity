package co.com.travelocity.utils;

public interface Builder <T>{
    T builder();
}
