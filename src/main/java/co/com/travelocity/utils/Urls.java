package co.com.travelocity.utils;

public enum Urls {
    URL_TRAVELOCITY("https://www.travelocity.com/");

    private final String url;

    Urls(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
