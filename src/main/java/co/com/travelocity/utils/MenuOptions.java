package co.com.travelocity.utils;

public enum MenuOptions {
    SIGN_IN("Sign in"),
    FLIGHTS("Flights"),
    MULTI_CITY("Multi-city");

    private final String value;

    MenuOptions(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
