package co.com.travelocity.utils;

public class Constant {

    public static final String DATE_FORMAT = "MMMM-yyyy-d";
    public static final String ROUND_TRIP = "Roundtrip";

    private Constant() {
    }
}
