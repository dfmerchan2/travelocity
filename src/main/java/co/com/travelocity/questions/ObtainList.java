package co.com.travelocity.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import net.serenitybdd.screenplay.targets.Target;

import java.util.List;

public class ObtainList implements Question<List<String>> {

    private final Target element;

    public ObtainList(Target element) {
        this.element = element;
    }

    @Override
    public List<String> answeredBy(Actor actor) {
        return Text.of(element).viewedBy(actor).asList();
    }

    public static ObtainList is(Target element) {
        return new ObtainList(element);
    }
}