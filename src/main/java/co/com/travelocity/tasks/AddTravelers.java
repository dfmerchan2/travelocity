package co.com.travelocity.tasks;

import co.com.travelocity.interactions.SelectQuantity;
import co.com.travelocity.models.Traveler;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.travelocity.userinterfaces.TravelerPage.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class AddTravelers implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        Traveler traveler = new Traveler();
        actor.attemptsTo(
                Click.on(BTN_TRAVELER),
                SelectQuantity.ofTravelers(LBL_NUMBER_ADULT, BTN_INCREASE_NUMBER_ADULT)
                        .withQuantity(traveler.getQuantityAdults()),
                SelectQuantity.ofTravelers(LBL_NUMBER_CHILDREN, BTN_INCREASE_NUMBER_CHILDREN)
                        .withQuantity(traveler.getQuantityChildren())
                        .withListAges(LST_AGE_CHILDREN)
                        .withButtonAges(BTN_AGE_CHILDREN)
                        .withOptionButtonAges(BTN_OPTION_AGE_CHILDREN),
                SelectQuantity.ofTravelers(LBL_NUMBER_INFANTS, BTN_INCREASE_NUMBER_INFANTS)
                        .withQuantity(traveler.getQuantityInfants())
                        .withListAges(LST_AGE_INFANT)
                        .withButtonAges(BTN_AGE_INFANT)
                        .withOptionButtonAges(BTN_OPTION_AGE_INFANT)

        );
    }

    public static Performable toBook() {
        return instrumented(AddTravelers.class);
    }
}
