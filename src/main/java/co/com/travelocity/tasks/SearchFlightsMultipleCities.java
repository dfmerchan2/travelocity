package co.com.travelocity.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Scroll;
import net.serenitybdd.screenplay.conditions.Check;
import org.openqa.selenium.Keys;

import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import static co.com.travelocity.userinterfaces.FlightsPage.*;
import static co.com.travelocity.userinterfaces.MenuPage.MENU_BUTTON;
import static co.com.travelocity.utils.MenuOptions.*;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class SearchFlightsMultipleCities implements Task {

    private List<Map<String, String>> data;
    private String classType;

    public SearchFlightsMultipleCities(List<Map<String, String>> data, String classType) {
        this.data = data;
        this.classType = classType;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(MENU_BUTTON.of(FLIGHTS.getValue())), Click.on(BTN_FLIGHT_OPTIONS.of(MULTI_CITY.getValue())));
        AtomicInteger j = new AtomicInteger(1);
        data.forEach(
                i -> {
                    actor.attemptsTo(
                            Check.whether(!BTN_ORIGIN_CITY.of(String.valueOf(j.get())).resolveFor(actor).isPresent())
                                    .andIfSo(Scroll.to(BTN_ADD_FLIGHT), Click.on(BTN_ADD_FLIGHT)),
                            Click.on(BTN_ORIGIN_CITY.of(String.valueOf(j.get()))),
                            Enter.theValue(i.get("originCity")).into(TXT_ORIGIN_CITY.of(String.valueOf(j.get()))).thenHit(Keys.ENTER),
                            Click.on(BTN_DESTINATION_CITY.of(String.valueOf(j.get()))),
                            Enter.theValue(i.get("destinationCity")).into(TXT_DESTINATION_CITY.of(String.valueOf(j.get()))).thenHit(Keys.ENTER));
                    j.getAndIncrement();
                }
        );
        actor.attemptsTo(
                Scroll.to(BTN_OPTIONS_CLASS),
                Click.on(BTN_OPTIONS_CLASS),
                Click.on(LST_OPTIONS_CLASS.of(classType))
        );

    }

    public static Performable inTheSystem(List<Map<String, String>> data, String classType) {
        return instrumented(SearchFlightsMultipleCities.class, data, classType);
    }
}
