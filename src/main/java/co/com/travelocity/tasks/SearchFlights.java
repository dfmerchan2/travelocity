package co.com.travelocity.tasks;

import co.com.travelocity.interactions.SelectDate;
import co.com.travelocity.models.Flight;
import co.com.travelocity.models.builder.FlightBuilder;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.conditions.Check;
import org.openqa.selenium.Keys;

import static co.com.travelocity.userinterfaces.Calendar.BTN_DEPARTING_DATE;
import static co.com.travelocity.userinterfaces.Calendar.BTN_RETURN_DATE;
import static co.com.travelocity.userinterfaces.FlightsPage.*;
import static co.com.travelocity.userinterfaces.MenuPage.MENU_BUTTON;
import static co.com.travelocity.utils.Constant.ROUND_TRIP;
import static co.com.travelocity.utils.MenuOptions.FLIGHTS;
import static co.com.travelocity.utils.Utilities.getDate;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class SearchFlights implements Task {

    private Flight flight;

    public SearchFlights(Flight flight) {
        this.flight = flight;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(MENU_BUTTON.of(FLIGHTS.getValue())),
                Click.on(BTN_FLIGHT_OPTIONS.of(flight.getFlightOptions())),
                Click.on(BTN_ORIGIN_CITY.of("1")),
                Enter.theValue(flight.getOriginCity()).into(TXT_ORIGIN_CITY.of("1")).thenHit(Keys.ENTER),
                Click.on(BTN_DESTINATION_CITY.of("1")),
                Enter.theValue(flight.getDestinationCity()).into(TXT_DESTINATION_CITY.of("1")).thenHit(Keys.ENTER),
                SelectDate.onCalendar(BTN_DEPARTING_DATE)
                        .withDate(getDate(flight.getInitialDate())),
                Check.whether(flight.getFlightOptions().equals(ROUND_TRIP))
                        .andIfSo(
                                SelectDate.onCalendar(BTN_RETURN_DATE)
                                        .withDate(getDate(flight.getFinalDate()))),
                Click.on(BTN_OPTIONS_CLASS),
                Click.on(LST_OPTIONS_CLASS.of(flight.getClassType()))
        );
    }

    public static Performable inTheSystem(FlightBuilder flightBuilder) {
        return instrumented(SearchFlights.class, flightBuilder.builder());
    }
}
