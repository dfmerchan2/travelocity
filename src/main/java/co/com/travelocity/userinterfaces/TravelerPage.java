package co.com.travelocity.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class TravelerPage {

    public static final Target BTN_TRAVELER =
            Target.the("Traveler Button ").located(By.id("adaptive-menu"));
    public static final Target LBL_NUMBER_ADULT =
            Target.the("Text number of adult").locatedBy("//input[starts-with(@id,'adult-input')]");
    public static final Target LBL_NUMBER_CHILDREN =
            Target.the("Text Number of children").locatedBy("//input[starts-with(@id,'child-input')]");
    public static final Target LBL_NUMBER_INFANTS =
            Target.the("Text Number of infant.").locatedBy("//input[starts-with(@id,'infant-input')]");
    public static final Target BTN_INCREASE_NUMBER_ADULT =
            Target.the("Increase number of adult button").locatedBy("//input[starts-with(@id,'adult-input')]/following-sibling::button");
    public static final Target BTN_INCREASE_NUMBER_CHILDREN =
            Target.the("Increase Number of children button").locatedBy("//input[starts-with(@id,'child-input')]/following-sibling::button");
    public static final Target BTN_INCREASE_NUMBER_INFANTS =
            Target.the("Increase Number of infants button").locatedBy("//input[starts-with(@id,'infant-input')]/following-sibling::button");
    public static final Target LST_AGE_CHILDREN =
            Target.the("List Age button children").locatedBy("//select[@id='child-age-input-0-{0}']//option");
    public static final Target BTN_AGE_CHILDREN =
            Target.the("Age button children").locatedBy("//select[@id='child-age-input-0-{0}']");
    public static final Target BTN_OPTION_AGE_CHILDREN =
            Target.the("Age button children").locatedBy("//select[@id='child-age-input-0-{0}']//option[@value='{1}']");
    public static final Target LST_AGE_INFANT =
            Target.the("List Age button infant").locatedBy("//select[@id='infant-age-input-0-{0}']//option");
    public static final Target BTN_AGE_INFANT =
            Target.the("Age button infant").locatedBy("//select[@id='infant-age-input-0-{0}']");
    public static final Target BTN_OPTION_AGE_INFANT =
            Target.the("Age button infant").locatedBy("//select[@id='infant-age-input-0-{0}']//option[contains(text(),'{1}')]");

    private TravelerPage() {
    }
}
