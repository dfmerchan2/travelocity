package co.com.travelocity.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class FlightsPage {

    public static final Target BTN_FLIGHT_OPTIONS =
            Target.the("Flight options button").locatedBy("//span[@class='uitk-tab-text' and contains(text(),'{0}')]");

    public static final Target BTN_ORIGIN_CITY =
            Target.the("origin city button").locatedBy("//div[@id='location-field-leg{0}-origin-menu']");
    public static final Target TXT_ORIGIN_CITY =
            Target.the("origin city field").locatedBy("//input[@id='location-field-leg{0}-origin']");
    public static final Target BTN_DESTINATION_CITY =
            Target.the("destination city button").locatedBy("//div[@id='location-field-leg{0}-destination-menu']");
    public static final Target TXT_DESTINATION_CITY =
            Target.the("destination city field").locatedBy("//input[@id='location-field-leg{0}-destination']");

    public static final Target BTN_OPTIONS_CLASS =
            Target.the("Options Class Types button").located(By.id("preferred-class-input-trigger"));
    public static final Target LST_OPTIONS_CLASS =
            Target.the("Options Class Types list").locatedBy("//span[@class='uitk-menu-list-item-label' and contains (text(),'{0}')]");

    public static final Target BTN_ADD_FLIGHT =
            Target.the("Add Another Flight").locatedBy("//button[@type='button' and contains (text(),'Add another flight')]");



    private FlightsPage() {
    }
}
