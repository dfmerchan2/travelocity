package co.com.travelocity.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;

public class MenuPage {
    public static final Target MENU_BUTTON =
            Target.the("Button sub menu").locatedBy("//span[@class='uitk-tab-text' and contains (text(),'{0}')]");

    private MenuPage() {
    }
}
