package co.com.travelocity.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class Calendar {

    public static final Target BTN_DEPARTING_DATE =
            Target.the("Departing date button").located(By.id("d1-btn"));
    public static final Target BTN_RETURN_DATE =
            Target.the("Return date button").located(By.id("d2-btn"));
    public static final Target LBL_DATE =
            Target.the("Label date").locatedBy("(//div[@class='uitk-calendar']//h2)[1]");
    public static final Target BTN_NEXT =
            Target.the("Next button").locatedBy("//button[@type='button']//*[@aria-labelledby='nextMonth-title']");
    public static final Target BTN_CALENDAR_DAY =
            Target.the("Calendar day button").locatedBy("(//button[@data-day='{0}'])[1]");
    public static final Target BTN_DONE =
            Target.the("Done button").locatedBy("//button[text()='Done']//span");

    private Calendar() {
    }
}
