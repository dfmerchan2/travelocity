package co.com.travelocity.models.builder;

import co.com.travelocity.models.Flight;
import co.com.travelocity.utils.Builder;

public class FlightBuilder implements Builder<Flight> {

    private String flightOptions;
    private String originCity;
    private String destinationCity;
    private String classType;

    public static FlightBuilder flightBuilder(){
        return new FlightBuilder();
    }

    public FlightBuilder withFlightOptions(String flightOptions) {
        this.flightOptions = flightOptions;
        return this;
    }

    public FlightBuilder withOriginCity(String originCity) {
        this.originCity = originCity;
        return this;
    }

    public FlightBuilder withDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
        return this;
    }

    public FlightBuilder withClassType(String classType) {
        this.classType = classType;
        return this;
    }

    public String getFlightOptions() {
        return flightOptions;
    }

    public String getOriginCity() {
        return originCity;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public String getClassType() {
        return classType;
    }

    @Override
    public Flight builder() {
        return new Flight(this);
    }
}
