package co.com.travelocity.models;

import com.github.javafaker.Faker;

public class Traveler {

    private Faker faker = new Faker();
    private int quantityAdults;
    private int quantityChildren;
    private int quantityInfants;

    public Traveler() {
        this.quantityAdults = faker.random().nextInt(1, 3);
        this.quantityChildren = faker.random().nextInt(0, 2);
        this.quantityInfants = faker.random().nextInt(0, 2);
    }

    public int getQuantityAdults() {
        return quantityAdults;
    }

    public int getQuantityChildren() {
        return quantityChildren;
    }

    public int getQuantityInfants() {
        return quantityInfants;
    }

}
