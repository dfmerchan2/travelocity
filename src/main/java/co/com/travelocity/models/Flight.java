package co.com.travelocity.models;

import co.com.travelocity.models.builder.FlightBuilder;
import com.github.javafaker.Faker;

public class Flight {

    private Faker faker = new Faker();
    private String flightOptions;
    private String originCity;
    private String destinationCity;
    private String classType;
    private int initialDate;
    private int finalDate;

    public Flight(FlightBuilder dataFlightBuilder) {
        this.flightOptions = dataFlightBuilder.getFlightOptions();
        this.originCity = dataFlightBuilder.getOriginCity();
        this.destinationCity = dataFlightBuilder.getDestinationCity();
        this.classType = dataFlightBuilder.getClassType();
        this.initialDate = faker.random().nextInt(1, 5);
        this.finalDate = faker.random().nextInt(6, 10);
    }

    public String getFlightOptions() {
        return flightOptions;
    }

    public String getOriginCity() {
        return originCity;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public String getClassType() {
        return classType;
    }

    public int getInitialDate() {
        return initialDate;
    }

    public int getFinalDate() {
        return finalDate;
    }
}
