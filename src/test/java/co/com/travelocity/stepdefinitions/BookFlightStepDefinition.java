package co.com.travelocity.stepdefinitions;

import co.com.travelocity.interactions.OpenPlatform;
import co.com.travelocity.tasks.AddTravelers;
import co.com.travelocity.tasks.SearchFlights;
import co.com.travelocity.tasks.SearchFlightsMultipleCities;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import net.serenitybdd.screenplay.Actor;

import java.util.List;
import java.util.Map;

import static co.com.travelocity.models.builder.FlightBuilder.flightBuilder;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class BookFlightStepDefinition {

    @Given("that Diego is looking for a {string} flight from {string} to {string} in {string} class")
    public void thatIsLookingForAFlightFromToNewYorkInClass(String flightOptions, String originCity, String destinationCity, String classType) {
        theActorCalled("diego").attemptsTo(
                OpenPlatform.ofTravelocity(),
                SearchFlights.inTheSystem(flightBuilder()
                        .withFlightOptions(flightOptions)
                        .withOriginCity(originCity)
                        .withDestinationCity(destinationCity)
                        .withClassType(classType)
                )
        );
    }

    @Given("that Diego looks for flights with multiple cities in {string} class")
    public void thatDiegoLooksForFlightsWithMultipleCities(String classType, List<Map<String, String>> data) {
        theActorCalled("diego").attemptsTo(
                OpenPlatform.ofTravelocity(),
                SearchFlightsMultipleCities.inTheSystem(data, classType)
        );
    }

    @And("he select the number of guests for the room")
    public void heSelectTheNumberOfGuestsForTheRoom() {
        theActorInTheSpotlight().attemptsTo(
                AddTravelers.toBook()
        );
    }


}
