package co.com.travelocity.stepdefinitions;

import io.cucumber.java.Before;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actors.Cast;
import net.serenitybdd.screenplay.actors.OnStage;
import org.openqa.selenium.WebDriver;

import static co.com.travelocity.utils.Utilities.getRandomDriver;
import static net.thucydides.core.webdriver.ThucydidesWebDriverSupport.getWebdriverManager;

public class Hook {

    private WebDriver driver = getWebdriverManager().getWebdriver(getRandomDriver());

    @Before
    public void setStage() {
        OnStage.setTheStage(
                Cast.whereEveryoneCan(BrowseTheWeb.with(driver)));
    }
}
