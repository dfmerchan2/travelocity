#Autor: Diego Fernando Merchan Jimenez

Feature: Create new users in Travelocity
  As a client of the Travelocity platform
  I want to create a user on the platform
  To access the different services offered

  Scenario Outline: Search flights from '<flightOptions>' from '<originCity>' to '<destinationCity>' in the class '<classType>'
    Given that Diego is looking for a '<flightOptions>' flight from '<originCity>' to '<destinationCity>' in '<classType>' class
    And he select the number of guests for the room
    Examples:
      | flightOptions | originCity     | destinationCity | classType       |
      | Roundtrip     | Bogota         | New York        | Premium economy |
      | One-way       | Rio de Janeiro | Medellin        | First class     |

  Scenario: Search flights with multiple cities
    Given that Diego looks for flights with multiple cities in 'Premium economy' class
      | originCity | destinationCity |
      | Argentina  | Toronto         |

      | Roma       | Medellin        |
      | Panama     | London          |
      | Peru       | Tokyo           |
    And he select the number of guests for the room